public class HelloWrold {

	public static void main(String[] args) {

		int n = Integer.parseInt(args[0]);
		double sum = 0.0;
		
		for (int i = 1; i < n; i++) {
			double temp = (double)1/factorialSum(i) ;
			sum = sum + temp;
		}
		System.out.println(sum);
	}
	
	/*This is the recursion way for calculating factorial(n)
	 * You don't have to know how it works just yet, but it is good to know.
	 * In your later course, you will learn how it works.*/
	private static int factorialSum(int number) {
		int result;
		if ( number == 1) {
			return 1;
		} else {
			result = factorialSum(number - 1) * number;
			return result;
		}

	}
	
	/*This is the normal way for calculating factorial(n) using loop.
	 * You can think of converting it to use for loop. Up to you*/
	private static int factorialSumCalcul(int number) {
		int sum = 1;
		while(number != 1) {
			sum = sum*number--;
		}
		return sum;
	}

}
