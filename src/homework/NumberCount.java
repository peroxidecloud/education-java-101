package homework;

import java.util.HashMap;

public class NumberCount {

	public static void main(String[] args) {
		int[] upperCount = new int[26];
		int[] lowerCount = new int[26];
		  for (int i = 0; i < upperCount.length; i++) {
	          upperCount[i] = 0;
	          lowerCount[i] = 0;
	      }
		  
		//String[] input = new String [args.length];
		

	    // Ask user to provide word
	  //  System.out.println("Please provide sentence you want to count:");
	  //  Scanner scanner = new Scanner(System.in);
	   // String input = scanner.next();
	    
		 // A list of uppercase letter and lowercase letter
		String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String lower = "abcdefghijklmnopqrstuvwxyz";
		
		String longText = "";
		for(String s:args) {
			longText += s;
		}
		
		// Count char
	    for (int i = 0; i < longText.length(); i++) {
	        char currentChar = longText.charAt(i);
	        int upperLetter = upper.indexOf(currentChar);
	        int lowerLetter = lower.indexOf(currentChar);
	        if (upperLetter != -1)
	            upperCount[upperLetter]++;
	        else if (lowerLetter != -1)
	            lowerCount[lowerLetter]++;
	    }

		
	    for (int i = 0; i < upperCount.length; i++) {
	        if (upperCount[i] != 0)
	            System.out.println(upper.charAt(i) + ": " + upperCount[i] + " times.");
	    }
	   
	    for (int i = 0; i < lowerCount.length; i++) {
	        if (lowerCount[i] != 0)
	            System.out.println(lower.charAt(i) + ": " + lowerCount[i] + " times.");
	    }
	}
}
