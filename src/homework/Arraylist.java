package homework;

import java.util.ArrayList;
import java.util.Scanner;

public class Arraylist {

	private ArrayList<String> input;

	public Arraylist(){
		input =new ArrayList<String>();
	}
	// Read text input from scanner input, adding each word into input array list
	public void readInput(){
		Scanner scn=new Scanner(System.in);
		System.out.println("Please enter text: ");
		String word="";
		
		while(scn.hasNextLine()) {
			word=scn.nextLine();			
			if(word.isEmpty()) {
				break;
			}
			String[] wordInput=word.split("\\W");
			
			for (int i=0;i<wordInput.length;i++) {
				input.add(wordInput[i]);
			}
		}
		scn.close();
	}
	
	
	// search the Arrayist for a particular word (specified by a parameter) that prints the location of all of the occurrences of the word.
	public void searchInput(String s) {
		for (int i=0;i<input.size();i++) {
			if(s.equals(input.get(i))) {
				System.out.print(input.get(i)+" occurs in: " +i );	
			}
		}
	}
	
	
	
   //returns a new ArrayList with the words at the positions specified by an int[]
	public ArrayList<String> newArrayList(int[] a) {
		ArrayList<String> newList= new ArrayList<String>();
		for(int i=0;i<a.length;i++) {
			if(a[i]>=input.size()) {
				System.out.println("You don't have this many words!");
				return null;
				}
			newList.add(input.get(a[i]));
		}
		
		return newList;
	}
	 
	
	public ArrayList<String> getInput() {
		return input;
	}

	public void setInput(ArrayList<String> input) {
		this.input = input;
	}
	

	
	
	
}
