package homework;

import java.util.Scanner;

public class DateFormatRon {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			Scanner scn = new Scanner(System.in);
			System.out.println("Please enter a date (mm-dd-yyyy)");
			while(scn.hasNextLine()){
				String input = scn.nextLine();
				//spliting the input string with "-"
				String[] date = input.split("-");
				//create a boolean variable and initialize it as true
				int day = Integer.parseInt(date[0]);
				int month = Integer.parseInt(date[1]);
				int year = Integer.parseInt(date[2]);
				
				boolean checkDate = true; //check again, you initialized this flag to be true. then when will this flag be set to false?
				boolean isCorrect = true;
				
				
				if(date[2].length() == 4 && date[1].length()==2 && date[0].length()==2) {
							
					switch(month){
							case 1: 
							case 3:
							case 5:
							case 7:
							case 8:
							case 10:
							case 12:
								if(day<32 && day > 0){
									checkDate = true;
								}
								break;
							case 4:
							case 6:
							case 9:
							case 11:
								if(day<31 && day > 0) {
							    		checkDate = true;
							    }
							    break;
							case 2:
								if(day < 29 && day > 0) { 
								    checkDate = true;
								}
								else if(day == 29 && year % 4==0) {
									checkDate = true;
								}
								break;
							default:
							}
				}
			    if(checkDate) {

		    			for(String section:date) {
		    				if(!checkDigit(section)) {
		    					isCorrect = false;
		    				}
		    			}
		    				System.out.println(isCorrect);
				    }
			    else{
			    			System.out.println(!isCorrect);
		            }
			scn.close();
	    }
	}
	       
		
		private static boolean checkDigit(String section) {
			char [] chars = section.toCharArray();
			for(char c:chars) {
				if(!Character.isDigit(c)) {
					return false;
				}
			}
			return true;
		}



	

}
