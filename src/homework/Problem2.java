package homework;

import java.util.ArrayList;
import java.util.Scanner;

public class Problem2 {
	
	public static void main  (String [] args) {
		Scanner scn = new Scanner(System.in);
		ArrayList<String> nameList =new ArrayList<String>();
		while(scn.hasNextLine()) {
			String name = scn.nextLine();
			char [] chars = name.toCharArray();
			boolean isName = true;
			for(char c:chars) {
				if(!Character.isLetter(c)) {
					isName = false;
				}
			}
			if(isName) {
				nameList.add(name);
			}
		}
		for(String n:nameList) {
			System.out.println(n);
		}
		
		scn.close();
	}
}
