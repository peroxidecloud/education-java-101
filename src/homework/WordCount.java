package homework;

import java.util.Scanner;

public class WordCount {

	public static void main(String [] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter a paragrah: ");
		String paragraph = input.nextLine();
		System.out.println("Please enter a word you would like to count: ");
		String word = input.next();
		

		int wordCount = 0;
		//find the word appearance from paragraph index
		int wordIndex=paragraph.indexOf(word);
		while (wordIndex>=0){
			wordIndex = paragraph.indexOf(word, wordIndex+1);
			wordCount++;
		}
		System.out.println("The paragragh you entered is: "+ paragraph);
		System.out.printf("The string %s was found %d times.\n",word, wordCount);
	}
	
}
