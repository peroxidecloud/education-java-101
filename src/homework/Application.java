package homework;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
	
		Scanner scan;
		try {
			scan = new Scanner(new File("/Users/rong-at-work/Desktop/data.csv"));
			while(scan.hasNextLine()) {
				String [] data = scan.nextLine().split(",");
				System.out.print(String.format("{\n'vlan_number':'%1s',\n'account_id\':' ',\n'vif_id':' ',\n'vif_name':' ',\n'tfnsw_ip':'%2s/30',\n'amazon_ip':'%3s/30',\n'status':false,\n'vif_location':'GbSY6'\n},\n",data[0].trim(),data[1].trim(),data[2].trim()));
			}
			scan.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}

