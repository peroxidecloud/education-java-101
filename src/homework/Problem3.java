package homework;

import java.util.Scanner;

public class Problem3 {

	public static void main(String [] args) {
		
			
		Scanner scn = new Scanner(System.in);
		
		while(scn.hasNextLine()) {
			String input = scn.nextLine();
			String [] date = input.split("-");
			boolean isCorrect = true;
			if(date.length !=3) {
				isCorrect = false;
			}
			for(String section:date) {
				if(!checkDigit(section)) {
					isCorrect = false;
				}
			}
			System.out.println(isCorrect);
		}
		scn.close();
	}

	private static boolean checkDigit(String section) {
		char [] chars = section.toCharArray();
		for(char c:chars) {
			if(!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}


}
