package homework;

public class Triangle {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//read the size of the array bt command-line input
		int i=Integer.parseInt(args[0]);
		int[][] pascalTriangle=new int[i][i];
		
		
		for (int n=0; n<i;n++){
			
			pascalTriangle[n][0]=pascalTriangle[n][n]=1;
			for (int m=1; m<n;m++){
				pascalTriangle[n][m]=pascalTriangle[n-1][m-1]+pascalTriangle[n-1][m];
			}
		}
		
		
		//print each row from traingle 
		for(int[] k: pascalTriangle){
			// print each row 
			for (int o=0; o<k.length; o++){
				// get rid out of following 0 from each row
				if (k[o]!= 0){
					System.out.print(k[o]+"  ");
				} 
				
			}
			System.out.println();
		}
		
	}
}
