package com.helloworld;

public class HelloWrold {

	public static void main(String[] args) {

		int a = doSum();
		int b = doSum();
		int c = doSum();
			
	}
	
	private static int doSum() {
		return 4+4;
	}
	
	/*This is the recursion way for calculating factorial(n)
	 * You don't have to know how it works just yet, but it is good to know.
	 * In your later course, you will learn how it works.*/
	private static int factorialSum(int number) {
		int result;
		if ( number == 1) {
			return 1;
		} else {
			result = factorialSum(number - 1) * number;
			return result;
		}

	}
	
	/*This is the normal way for calculating factorial(n) using loop.
	 * You can think of converting it to use for loop. Up to you*/
	private static int factorialSumCalcul(int number) {
		int sum = 1;
		while(number != 1) {
			sum = sum*number--;
		}
		return sum;
	}

}
