package com.education.basic.object;

public class ScienceCalculator extends Calculator {

	public ScienceCalculator(String calculatorName) {
		super(calculatorName);

	}
	
	public String getCalculatorType() {
		return "Sciece Calculator";
	}
}
