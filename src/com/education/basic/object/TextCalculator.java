package com.education.basic.object;

public class TextCalculator extends Calculator {

	public TextCalculator(String calculatorName) {
		super(calculatorName);
	}
	
	public String getCalculatorType() {
		return "Text Calculator";
	}

}
