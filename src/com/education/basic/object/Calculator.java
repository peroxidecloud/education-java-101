package com.education.basic.object;

/**
 * @author develop
 * This is a very basic class represent Calculator. 
 * It has a few member fields that describes some basic property of a Calculator
 * It has a few public method to allow others to operate on its created object.
 */
public class Calculator{

	/* Following are called class member fields
	 * We declare them to be private so that the only way to access these fields are through public method.
	 * These fields are 'encapsulated' within class with only one access entry point through method
	 * */
	private String calculatorName;
	private int serialNumber;
	private int finalResult=0;
	
	/* This is a constructor method. 
	 * This will return a copy of new Calculator object with the specified calculator name */
	public Calculator(final String calculatorName){
		this.calculatorName = calculatorName;
	}
	
	/* This is a public method. 
	 * The public method is the operation of a Class can have to operate on its member fields*/
	public double doSum(int ... numbers){
		resetFinalResult();
		for (int num:numbers){
			finalResult += num;
		}
		return finalResult;
	}
	
	/*another public method*/
	public double doSubtraction(int initialNumber, int... substractions) {
		resetFinalResult();
		for (int num:substractions){
			initialNumber -= num;
		}
		finalResult = initialNumber;
		return finalResult;
	}

	/*This is a private method.
	 * A private method is only visible within class and helping the class to achieve the desired public operations*/
	private void resetFinalResult() {
		finalResult = 0;
	}
	public String getCalculatorName() {
		return calculatorName;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	public String getCalculatorType() {
		return "Normal Calculator";
	}

}
