package com.education.basic.object;

public class CalculatorFactory {

//	public static Calculator createDefaultCalculator(String name) {
//		return new Calculator(name);
//	}
//	
//	public static Calculator createScienceCalculator(String name) {
//		return new ScienceCalculator(name);
//	}
//	
//	public static Calculator createTextCalculator(String name) {
//		return new TextCalculator(name);
//	}
	
	public static Calculator createCalculator(String type) {
		if (type.equalsIgnoreCase("Normal")){
			 return new Calculator(type);
		}
		
		if (type.equalsIgnoreCase("Science")){
			 return new ScienceCalculator(type);
		}
		
		if (type.equalsIgnoreCase("Text")){
			 return new TextCalculator(type);
		}
		return new Calculator("Normal");
	}
}
