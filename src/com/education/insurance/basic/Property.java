package com.education.insurance.basic;

import java.util.ArrayList;

public class Property implements Risk{
	
	
	private String postCode;
	private int age;
	private String material;
	private final String type = "PROPERTY";
	
	
	public Property(String postCode, int age, String material) {
		super();
		this.postCode = postCode;
		this.age = age;
		this.material = material;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	
	
	@Override
	public String getType() {
		return this.type;
	}

	@Override
	public ArrayList<String> getPricingFactors() {
		ArrayList<String> factors = new ArrayList<String>();
		factors.add(this.getMaterial());
		factors.add(Integer.toString(this.getAge()));
		factors.add(this.getPostCode());
		return factors;
	}

}
