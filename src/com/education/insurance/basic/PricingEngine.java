package com.education.insurance.basic;

import java.util.ArrayList;

public class PricingEngine {
	
	public static PricingEngine services() {
		return new PricingEngine();
	}
	
	private  PricingEngine() {
		
	}
	
	public double getPrice(Quote quote) {
		double finalPrice = 0.0;
		for (Risk risk:quote.getRisk()){			
			finalPrice += ratingRule(risk.getPricingFactors(),risk.getType());
		}
		return finalPrice;
	}
	
	
	
	
	private double ratingRule(ArrayList<String> ratingFactore,String riskType) {
		if (riskType == "CAR") {
			return 100*2;
		}else {
			return 100*3.0;
		}
		
	}

}
