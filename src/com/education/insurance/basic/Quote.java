package com.education.insurance.basic;

import java.util.ArrayList;

public class Quote {
	
	private Person person;
	private String quoteType;
	private ArrayList<Risk> riskList;
	
	
	public String getQuoteType() {
		return quoteType;
	}

//	public void setQuoteType(String quoteType) {
//		this.quoteType = quoteType;
//	}

	public ArrayList<Risk> getRisk() {
		return riskList;
	}

	public void setRisk(ArrayList<Risk> risk) {
		this.riskList = risk;
	}

	public Quote(Person person, String quoteType, ArrayList<Risk> riskList) {
		super();
		this.person = person;		
		this.riskList = riskList;
		this.quoteType = quoteType;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	
	
}
