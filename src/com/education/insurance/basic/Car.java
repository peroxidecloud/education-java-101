package com.education.insurance.basic;

import java.util.ArrayList;

public abstract class Car implements Risk{

	private String rego;
	private String kms;
	private int gvm;
	private String brand;
	private final String type = "CAR";
	
	
	public String getType() {
		return type;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Car(String rego, String kms, int gvm,String brand) {
		super();
		this.rego = rego;
		this.kms = kms;
		this.gvm = gvm;
		this.brand = brand;
	}
	
	public int getDoubleGvm(int ratio) {
		return gvm*ratio*2;
	}
	
	public int getDoubleGvm(double ratio) {
		return (int) (gvm*ratio*2);
	}
	
	public String getRego() {
		return rego;
	}
	public void setRego(String rego) {
		this.rego = rego;
	}
	public String getKms() {
		return kms;
	}
	public void setKms(String kms) {
		this.kms = kms;
	}
	public int getGvm() {
		return gvm;
	}
	public void setGvm(int gvm) {
		this.gvm = gvm;
	}
	
	
	public String toString() {
		return "Gvm: "+this.getGvm()+" Kms: "+this.getKms()+" Rego: "+this.getRego()+" brand:"+this.getBrand();
	}
	
	@Override
	public ArrayList<String> getPricingFactors() {
		ArrayList<String> factors = new ArrayList<String>();
		factors.add(this.getBrand());
		factors.add(this.getKms());
		return factors;
	}
}
