package com.education.insurance.basic;

import java.util.ArrayList;

public interface Risk {
	
	
	public String getType();
	
	public ArrayList<String> getPricingFactors();
}
