package com.iot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

public class SplitData {

	public static void main(String [] args) {
		String filePath = "/Users/rong-at-work/Project/CloudProject/IoT/iot-data-to-dynamodb/iot-lambdas/simulate-data/";
		File p = new File(filePath);
		ArrayList<File> nameList = new ArrayList<File>();
		int dirCount = 1;
		for (File f: p.listFiles()){
			if (!f.isDirectory()) {
				nameList.add(f);
			}
			if(nameList.size() == 10) {
				String newPath = filePath+"/"+Integer.toString(dirCount);
				File theDir = new File(newPath);
				theDir.mkdirs();
				for (File fn:nameList) {
					try {
						
						Files.move(Paths.get(fn.getAbsolutePath()), Paths.get(newPath+"/"+fn.getName()), StandardCopyOption.REPLACE_EXISTING);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				dirCount++;
				nameList.clear();
				
			}
			
		}
		
	
	}
}
