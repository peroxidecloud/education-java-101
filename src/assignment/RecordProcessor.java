package assignment;

import java.util.ArrayList;

public class RecordProcessor {

	public static Record create(ArrayList<String> recordLineTracker) {
		Record record = new Record();
		
		for(String line:recordLineTracker) {
			String [] recordFields = line.split("\\W"); // split the record by space
			String fieldName = recordFields[0];
			String fieldValue = recordFields[1];
			
			switch (fieldName) {
				case "Name":
					record.setName(fieldValue);
					break;
				case "OtherField":
					//record.setName(fieldValue); //set other filedValue use similar method
					break;
			}
		}
		return record;
	}
}
