package assignment;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class MainApplication {

	public static void main(String[] args) {

		ArrayList<Record> recordStore = new ArrayList<Record>(); // you can convert this to a store class if necessary
																	
		ArrayList<Instruction> instructionStore = new ArrayList<Instruction>();  //storing CRUD instructions
		ArrayList<Instruction> queryStore = new ArrayList<Instruction>(); //storing query instructions

		ArrayList<String> queryResults = new ArrayList<String>(); //storing query results
		
		try {
			Scanner recordScan = new Scanner(new File("Path/to/RecordFile"));
			Scanner instructionScan = new Scanner(new File("Path/to/InstructionFile"));

			/* First we read the record file line by line, then process each line to be a record */
			ArrayList<String> recordLineTracker = new ArrayList<String>();
			while (recordScan.hasNextLine()) {
				String textLine = recordScan.nextLine();
				if(textLine.isEmpty()) {
					/*
					 * This indicates a new record, therefore we need to
					 * 1. Create the new record using previous a few lines as input
					 * 2. Store the new record
					 * 3. Clean the recordLineTracker for next use 
					 */
					/*
					 * Remember let RecordProcessor to take the full control of creating a record
					 * Once a record is returned from processor, always assume it is a completed
					 * record without error This is the concept of 'Let one do one thing, and let it
					 * do well'
					 */
					Record newRecord = RecordProcessor.create(recordLineTracker);
					recordStore.add(newRecord); // let's store the record
					recordLineTracker.clear();
				}
				else {
					recordLineTracker.add(textLine); //Keep collecting lines as record
					}
				}
			recordScan.close();

			/*
			 * Second we read the instruction file line by line, then process each line to
			 * be a instruction We then storing 'CRUD' instruction and 'Query' instruction
			 * to 2 separated list
			 */
			while (instructionScan.hasNextLine()) {
				String[] rawInstruction = instructionScan.nextLine().split(",");
				Instruction newInstruction = InstructionProcessor.create(rawInstruction);
				if (newInstruction.getKeyWord().equalsIgnoreCase("query")) {
					queryStore.add(newInstruction);
				} else {
					instructionStore.add(newInstruction);
				}
			}
			instructionScan.close();
			
			/* Now we are starting the record CRUD operations first */
			for (Instruction crud : instructionStore) {
					InstructionExecutor.performInstruction(crud, recordStore);
				}
			
			/*writing records to file*/
			writeRecordToFile(recordStore);
			
			/* Now we perform sorting on the recordStore after the CRUD operations */
			sort(recordStore);

			/* Now we perform query on top of the sorted records */
			for (Instruction crud : instructionStore) {
				queryResults.add(InstructionExecutor.performQuery(crud, recordStore));
			}
			
			/* Now we writing query reports  to file*/
			writeReportToFile(queryResults);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private static void sort(ArrayList<Record> recordStore) {
		// TODO Auto-generated method stub

	}

	private static void writeRecordToFile(ArrayList<Record> recordStore) {
		// TODO Auto-generated method stub

	}
	
	private static void writeReportToFile(ArrayList<String> report) {
		// TODO Auto-generated method stub

	}

}