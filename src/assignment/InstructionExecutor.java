package assignment;

import java.util.ArrayList;

public class InstructionExecutor {
	
	/*We leave the actual record operation to this method*/
	public static void performInstruction(Instruction inst, ArrayList<Record> data) {
		switch (inst.getKeyWord()) {
			case "UPDATE":
				updateRecord(inst,data);
				break;
			case "DELETE":
				break;
			case "INSERT":
				break;
		}
		
		
	}
	
	private static void updateRecord(Instruction inst, ArrayList<Record> data) {
		String name = inst.getName(); 
		String phone = inst.getPhone();
		
		for (Record r:data) {
			if(r.getName().equalsIgnoreCase(name)) {
				r.setPhone(phone);
			}
		}
		
	}

	public static String performQuery(Instruction inst, ArrayList<Record> data) {
		String result = "";
		if(!inst.getKeyWord().equalsIgnoreCase("query")) {
			//do nothing
		}else {
			result = "query result from data"; 
			
		}
		return result;
	}
	
	
}
