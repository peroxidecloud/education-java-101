package assignment;

import java.util.ArrayList;

public class Instruction {

	private String keyWord; 
	private ArrayList<String> parameter = new ArrayList<String>(); 
	
	public Instruction(String keyWord, String ...params) {
		this.keyWord = keyWord;
		for(String p:params) {
			parameter.add(p);
		}
		
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public ArrayList<String> getParameter() {
		return parameter;
	}

	public void setParameter(ArrayList<String> parameter) {
		this.parameter = parameter;
	}
	
	public String getName() {
		return parameter.get(0);
	}
	
	public String getPhone() {
		return parameter.get(1);
	}
}
