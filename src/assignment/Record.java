package assignment;

public class Record {

	private String name = "";
	private String phone = "";
	private String postCode; 
	
	
	public Record() {}
	public Record( String postCode) {
		super();
		
		this.postCode = postCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	
	
}
