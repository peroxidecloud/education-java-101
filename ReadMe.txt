Understand Object Oriented Programming
- A programming methodology. 
- Use Object to represent and describe the real world item
- Let Objects interacting with each other through a set of per-defined protocols. 
- 4 Basic attributes of OOP. Encapsulation, Inheritance, Abstraction, Polymorphism
- We will only cover Encapsulation in this 101 using Java.
More Reading - https://en.wikipedia.org/wiki/Object-oriented_programming

What is Java
- Java is a programming language with OOP concepts built in.
- It does not mean you can not use other programming language to perform OOP . Java is just made it a bit easier.
- JDK (Java Development Kit). JDK is used to develop and compile Java code (text file) to byte code.
- JRE (Java Runtime Environment). JRE is the place for running and executing Java byte code 
- IDE (Integrated Development Environment). Eclipse is a popular IDE for writing Java application

What are Class, Object, and Instance?
- Class. A Template or a blueprint that abstractly describes the real world item
- Object. A creation of the Class.
- Instance. The live status of an Object.
- A very good analog from one of the clever Mum 
	- Class is the Cookie Cutter.
	- Object is the Cookie.
	- Instance is when you start eating the cookie.
More Reading - https://en.wikipedia.org/wiki/Class_(computer_programming)

Home Work 1 - 
Create a new Application class with main method.
Create a new Object from Calculator class with your desired name
Invoke all the public method from the Calculator class
Print all the method invocation results.

Home Work 2 -
Design a class with your own choice. Use your imagination; it could be cars, desks, TVs etc
The class needs to have 
	- a few private member fields 
	- a few public methods to describe the available class operations
	- 1 or 2 private method to understand its difference with public method
	- A constructor method for creating the object
	- An application class with main method and showing you can create and operating the object
	- Print out the results using system.out.println 

